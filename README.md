# Docker image for Peer-base Pinner Service


## 1. Create a docker image

```
docker build --no-cache -t stopcoronavirus/peer-base:deploy .
```

## 2. Test docker image 


```
docker run -it --rm \
  -e PEER_STAR_SWARM_ADDRESSES=/dns4/rdv.alpha-01.bootstrap.stopcoronavirus.tech/tcp/443/wss/p2p-websocket-star \
  -e PEER_STAR_BOOTSTRAP_ADDRESSES=/dns4/ipfs-gateway.alpha-01.bootstrap.stopcoronavirus.tech/tcp/4001/ipfs/QmXFeqdECoxEztvFfpJcTHj5zvBwfFFWEDVNu5uK5viSnb \
  -e PEER_STAR_APP_NAME=peer-pad/2 \
  -p "3001:3001" \
  stopcoronavirus/peer-base:deploy
```

## 2. Publish docker image 


```
docker push stopcoronavirus/peer-base:deploy
```

## 3. Deploy anywhere

```
docker run -d --name peer-base \
  -e PEER_STAR_SWARM_ADDRESSES=/dns4/rdv.alpha-01.bootstrap.stopcoronavirus.tech/tcp/443/wss/p2p-websocket-star \
  -e PEER_STAR_BOOTSTRAP_ADDRESSES=/dns4/ipfs-gateway.alpha-01.bootstrap.stopcoronavirus.tech/tcp/4001/ipfs/QmXFeqdECoxEztvFfpJcTHj5zvBwfFFWEDVNu5uK5viSnb \
  -e PEER_STAR_APP_NAME=peer-pad/2 \
  -v "$(pwd)/jsipfs:/root/.jsipfs" \
  -p "3001:3001" \
  stopcoronavirus/peer-base:deploy
```