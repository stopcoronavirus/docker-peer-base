FROM node:10

RUN apt update \
    && apt install -y git

RUN mkdir /app \
    && cd /app \
    && git clone --single-branch --branch deploy https://gitlab.com/stopcoronavirus/peer-base

RUN cd /app/peer-base \
    && npm install

WORKDIR /app/peer-base

ENV PEER_STAR_SWARM_ADDRESSES /dns4/rdv.alpha-01.bootstrap.stopcoronavirus.tech/tcp/443/wss/p2p-websocket-star
ENV PEER_STAR_BOOTSTRAP_ADDRESSES /dns4/ipfs-gateway.alpha-01.bootstrap.stopcoronavirus.tech/tcp/4001/ipfs/QmXFeqdECoxEztvFfpJcTHj5zvBwfFFWEDVNu5uK5viSnb
ENV PEER_STAR_APP_NAME peer-pad/2

CMD ["/app/peer-base/bin/pinner"]
